<?php
session_start();
include ('API\classes\logger\logger.php');
function my_autoloader($class) {
    include 'API/classes/' . $class . '.class.php';
}
spl_autoload_register('my_autoloader');
$log = Helper::getLogger();
$manager = new SqlManager();
if (isset($_SESSION['auth_token']) && $manager->isTokenValid($_SESSION['user_id'],$_SESSION['auth_token']))
{
//$log->info("hello213!");
?>
<!doctype html>
<html lang="en">
  <head>
    <?php 
        Template::getHeader();
    ?>
  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Home</a>
    </header>
    <div class="container-fluid">
        <div class="row">
            <?php 
            Template::getMenu();
            ?>
            <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                <h1>Gestione Questionari</h1>
                <?php
                //-----------------------------
                //CONTENT
                //-----------------------------
                ?>
            </main>
        </div>
    </div>
    <?php
        Template::getFooter();
    ?>
  </body>
</html>
<?php
}
else
{
    header("location: index.php");
}