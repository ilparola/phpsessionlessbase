<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
include ('API\classes\logger\logger.php');
function my_autoloader($class) {
    include 'API/classes/' . $class . '.class.php';
}
spl_autoload_register('my_autoloader');
$log = Helper::getLogger('Gestione Questionari');
$manager = new SqlManager();
if (isset($_SESSION['auth_token']) && $manager->isTokenValid($_SESSION['user_id'],$_SESSION['auth_token']))
{
    UserValidation::extendToken($_SESSION['user_id']);
    //$log->info("hello213!");
    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <?php 
            Template::getHeader();
        ?>
    </head>
    <body>
        <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Home</a>
        </header>
        <div class="container-fluid">
            <div class="row">
                <?php 
                Template::getMenu();
                ?>
                <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                    <h1>Gestione Questionari</h1>
                    <h3><a  class="btn btn-primary" data-toggle="collapse" href="#formcollapse" role="button" aria-expanded="false" aria-controls="formcollapse">+ Inserisci Questionario</a></h3>
                    <div class="collapse" id="formcollapse">
                        <form id="addquestionarioform">
                            <input type="text" name="titolo" class="form-control" required="required" placeholder="Titolo">
                            <input type="hidden" value="<?=$_SESSION['user_id']?>" name="user_id">
                            <br>
                            
                        </form>
                        <div class="card" id="response"></div>
                        <button class="btn btn-primary" id="inserisciQuestionario">Salva</button>
                    </div>
                    <hr>
                    <div id="listaquestionari">
                    <?php
                    $manager->isTokenValid($_SESSION['user_id'],$_SESSION['auth_token']);
                    Template::drwQuestionari();
                    
                    ?>
                    </div>
                </main>
            </div>
        </div>
        <?php
            Template::getFooter();
        ?>
    </body>
    </html>
    <script>
        $( "#inserisciQuestionario" ).on( "click", function() {
                res = $.post('API/addquestionario.php',$('#addquestionarioform').serialize(),function(data) {
                $("#response").html( data );
                $("#listaquestionari").load( "API/livepages/listquestionari.php");
                });
        });
        $(document).ready(function() {
            $('#questionari').DataTable();
        } );
        

    </script>
<?php
}
else
{
    header("location: index.php");
}
