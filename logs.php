<?php
/*
Level	Severity	Description
FATAL	Highest	Very severe error events that will presumably lead the application to abort.
ERROR	...	Error events that might still allow the application to continue running.
WARN	...	Potentially harmful situations which still allow the application to continue running.
INFO	...	Informational messages that highlight the progress of the application at coarse-grained level.
DEBUG	...	Fine-grained informational events that are most useful to debug an application.
TRACE	Lowest	Finest-grained informational events.
*/
session_start();
include ('API\classes\logger\logger.php');
function my_autoloader($class) {
    include 'API/classes/' . $class . '.class.php';
}
spl_autoload_register('my_autoloader');
$log = Helper::getLogger();
$manager = new SqlManager();
if (isset($_SESSION['auth_token']) && $manager->isTokenValid($_SESSION['user_id'],$_SESSION['auth_token']))
{
    UserValidation::extendToken($_SESSION['user_id']);
//$log->info("hello213!");
?>
    <!doctype html>
    <html lang="en">
    <head>
        <?php 
            Template::getHeader();
        ?>
    </head>
    <body>
        <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Home</a>
        </header>
        <div class="container-fluid">
            <div class="row">
                <?php 
                Template::getMenu();
                ?>
                <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                    <h1>
                        Logs 
                        <form method="POST" action="API/livepages/deletelogs.php">
                            <input type="hidden" name="categoria" value="INFO">
                            <button type="submit" class="btn btn-primary btn-sm">Elimina Info Logs</button>
                        
                            <button type="submit" class="btn btn-info btn-sm">Elimina Trace Logs</button>
                     
                            <button type="submit" class="btn btn-warning btn-sm">Elimina Warns Logs</button>
                      
                            <button type="submit" class="btn btn-danger btn-sm">Elimina Error Logs</button>
                        </form>
                        
                    </h1>
                    <?php
                    Template::drwaTableOf('log4php_log','logs',array());
                    ?>
                </main>
            </div>
        </div>
        <?php
            Template::getFooter();
        ?>
    </body>
    </html>
    <script>
    $(document).ready(function() {
        $('#logs').DataTable();
    } );
    </script>
<?php
}
else
{
    header("location: index.php");
}