<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include ('API\classes\logger\logger.php');
function my_autoloader($class) {
    include 'API/classes/' . $class . '.class.php';
}
spl_autoload_register('my_autoloader');
$log = Helper::getLogger();
$auth = new UserValidation();
$manager = new SqlManager();
//$log->info("hello213!");

if (isset($_POST['username']))
{
  $token = json_decode($auth->login($_POST['username'],$_POST['password']));
  //print_r($token);
  if ($token->result=="ok")
  {
    $_SESSION['auth_token'] = $token->data->token;
    $_SESSION['user_id'] = $token->data->user_id;
    header("location:gestionequestionari.php?tk:".$token->data->token."&id=".$token->data->user_id);
  }
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Gestione Questionari - Civita</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
  </head>

  <body class="text-center">
   </pre>
    <form class="form-signin" action="index.php" method="post">
      <img class="mb-4" src="img/civita.png" alt="" >
      <label for="inputEmail" class="sr-only">Username</label>
      <input name="username" type="text" id="inputEmail" class="form-control" placeholder="Username" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <div id="message"></div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Entra</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
    </form>
  </body>
  <?php
        Template::getFooter();
    ?>
</html>

<?php
if (isset($token->result))
{
?>
  <script>
  $("#message").html("<div class=\"alert alert-<?=$token->priority?> alert-dismissible fade show\" role=\"alert\"><strong><?=$token->message?></strong><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div>");
  </script>
<?php
}