<?php
include ('..\classes\logger\Logger.php');
function my_autoloader($class) {
    include '../classes/' . $class . '.class.php';
}
spl_autoload_register('my_autoloader');
$manager = new SqlManager();
$manager->openConnection();
$esito=$manager->deleteLogsTagged($_POST['categoria']);
$manager->closeConnection();
if (isset($_POST['categoria']))
{
    if ($esito)
    {
        header("location: ../../logs.php");
    }   
    else
    {
        header("location: ../../logs.php?err=Error");
    }
}
else
{
    header("location: ../../index.php");
}