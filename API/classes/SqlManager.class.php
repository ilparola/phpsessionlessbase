<?php
class SqlManager
{
	var $log;
	var $conn;
	function __construct()
	{
		$this->log = Helper::getLogger('SQL Logger');
		
	}

	public function openConnection()
	{
		try
		{
			$this->conn = new PDO("mysql:host=localhost;dbname=opera", "operaMySql", "TtOperaMySql", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
		}
		catch(PDOException $e)
		{
			$this->log->fatal($e->getMessage());
		}
	}
	public function closeConnection()
	{
		$this->conn=null;
	}
	public function checkUserPassword($username,$password)
	{
		$query="SELECT * FROM user WHERE username='$username'";
		$results=$this->conn->query($query);
		$array= $results->fetch(PDO::FETCH_ASSOC);
		if (empty($array)){
			return FALSE;
		}
		if (password_verify($password, $array['password']))
			return $array;
		else
			return FALSE;
	}
	public function isTokenValid($userid, $token)
	{
		$this->openConnection();
		$esito = $this->checkToken($userid, $token);
		$this->closeConnection();
		return $esito;
	}
	public function checkToken($userid,$token)
	{
		//$this->log->info("effettuo check del token per userId: ".$userid);
		$query="SELECT token_key , TIME_TO_SEC(TIMEDIFF(NOW(),token_exp))/60 as minutes from authtoken WHERE user_id='$userid'";
		$results=$this->conn->query($query);
		$array= $results->fetch(PDO::FETCH_ASSOC);
		if (empty($array)){
			$this->log->error("Query vuota: ".$query);
			return FALSE;
		}
		if ($array['token_key']==$token and $array['minutes'] < 120)
		{
			//$this->log->info("Token di minuti: ".$array['minutes']);
			return TRUE;
			
		}
		else
		{
			return FALSE;
		}
	}

	public function deleteLogsTagged($category)
	{
		
		$query = "DELETE FROM log4php_log where level = '".$category."'";
		try
		{
			
			$exec=$this->conn->prepare($query);
			$exec->execute();
			return true;
		}
		catch (Exception $e)
		{
			$this->log->error("Cancellazione Logs " . $e);
			return false;	
		}
	}

	public function saveAuthToken($userid,$token)
	{
		
		$query="DELETE FROM authtoken WHERE user_id='$userid'";
		$exec=$this->conn->prepare($query);
		$exec->execute();
		$timestamp=date('Y-m-d H:i:s');
		$query ="INSERT INTO authtoken (user_id, token_key, token_exp) VALUES ('$userid','$token','$timestamp')";
		$exec=$this->conn->prepare($query);
		$exec->execute();
		$exec=null;
	}
	public function updateExpDate($userId)
	{
		try{
			$query = "UPDATE authtoken set token_exp = NOW() where user_id = ".$userId;
			//$this->log->info("UPDATE TOKEN QUERY: ".$query);
			$exec=$this->conn->prepare($query);
			$exec->execute();
			return True;
		}
		catch(Exception $e)
		{
			return False;
		}
		
	}
	public function getUser($userid)
	{
		$query="SELECT * FROM user WHERE userId='$userid'";
		$results=$this->conn->query($query);
		$array=$results->fetch(PDO::FETCH_ASSOC);
		if (empty($array))
			return FALSE;
		else
			return $array;
	}
	public function checkUsername($username)
	{
		$query="SELECT * FROM user WHERE username='$username'";
		$results=$this->conn->query($query);
		$array=$results->fetch(PDO::FETCH_ASSOC);
		if (empty($array))
			return TRUE;
		else
			return FALSE;
	}
	public function selectStarFrom($table, $orderby = "")
	{
		$query= "SELECT * FROM ".$table." ".$orderby;
		$results=$this->conn->query($query);
		$dataSet = array();
		while($row = $results->fetch(PDO::FETCH_ASSOC))
		{
			array_push($dataSet,$row);
		}
		return $dataSet;
	}
	public function saveUser($user)
	{
		$this->log->info("Tentativo di aggiunta utente");
		$this->openConnection();
		try
		{
			$query = "INSERT INTO user (username,password,admin,email,nome,cognome) VALUES (:user ,:pasw , :adm ,:mail,:nome,:cogn)";
			$sth = $this->conn->prepare($query); 
			$sth->bindParam(':user', $user->username);
			$sth->bindParam(':pasw', $user->hash);
			$sth->bindParam(':adm', $user->admin);
			$sth->bindParam(':mail', $user->email);
			$sth->bindParam(':nome', $user->nome);
			$sth->bindParam(':cogn', $user->cognome);
			$sth->execute();
			$data = $sth->errorInfo();
			$this->closeConnection();
			$response = [
				'message' => !isset($data[2]) ? 'Utente inserito con successo!' : $data[2],
				'priority' => !isset($data[2]) ? 'success' : 'danger',
				'result' => !isset($data[2]) ? 'ok' : 'ko',
				'data' => $data
			];
			$this->log->info("Query inserimento utente effettuata");
			return json_encode($response);
		} 
		catch (Exception $ex) 
		{
			$this->log->error("Query inserimento utente non effettuata. Catch");
			$this->closeConnection();
			$response = [
				'message' => 'Utente non inserito: '.$ex,
				'priority' => 'danger',
				'result' => 'ko',
				'data' => $data
			];
			return json_encode($response);
		}
	}
	public function saveQuestionario($questionario)
	{
		$this->openConnection();
		try
		{
			$query = "INSERT INTO questionario (descrizione, id_utente) VALUES (:des, :user)";
			$sth = $this->conn->prepare($query); 
			$sth->bindParam(':des', $questionario->titolo);
			$sth->bindParam(':user', $questionario->userId);
			$sth->execute();
			$data = $sth->errorInfo();
			$this->closeConnection();
			$response = [
				'message' => !isset($data[2]) ? 'Questionario inserito con successo!' : $data[2],
				'priority' => !isset($data[2]) ? 'success' : 'danger',
				'result' => !isset($data[2]) ? 'ok' : 'ko',
				'data' => $data
			];
			return json_encode($response);
		} 
		catch (Exception $ex) 
		{
			$this->closeConnection();
			$response = [
				'message' => 'Questionario non inserito: '.$ex,
				'priority' => 'danger',
				'result' => 'ko',
				'data' => $data
			];
			return json_encode($response);
		}
	}
	


}
