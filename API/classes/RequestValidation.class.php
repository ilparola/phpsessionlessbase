<?php
class RequestValidation
{
	public function __invoke($request, $response, $next)
	{
		$sqlManager = new SqlManager;
        $sqlManager->openConnection();
        $auth=$sqlManager->checkToken($request->getParam('user_id'),$request->getParam('token'));
        $sqlManager->closeConnection();
		if ($auth)
			$response = $next($request, $response);
		else
			$response->getBody()->write("{'error':'token error'}");
		return $response;
	}

	public static function tokenIsValid($userId,$token)
	{
		$sqlManager = new SqlManager;
        $sqlManager->openConnection();
        $auth=$sqlManager->checkToken($userId,$token);
        $sqlManager->closeConnection();
		if ($auth)
			return true;
		else
			return false;
	}

}