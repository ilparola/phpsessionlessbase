<?php

class Template
{
    public static function getMenu()
    {
        ?>
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="gestionequestionari.php">Gestione Questionari </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="gestioneutenti.php">Gestione Utenti </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logs.php">Logs </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="API/livepages/logout.php">Esci </a>
                </li>
            </ul>
        </nav>
      <?php
    }
    
    
    public static function getHeader()
    {
        ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">
        <title>Gestione Questionari</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/dashboard.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <link href="css/select2.min.css" rel="stylesheet" />
        <?php
    }

    public static function showAlertInDiv($response)
    {
        $array = json_decode($response);
        ?>
        
        <div class="alert alert-<?=$array->priority?> alert-dismissible fade show" role="alert">
            
            <h4 class="alert-heading">Risultato:<?=$array->result?></h4>
            <p><?=$array->message?></p>
            <hr>
            <p class="mb-0"><?php print_r($array);?></p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </div>
        <?php
    }

    public static function getFooter()
    {
        ?>
        <!--<script>window.jQuery || document.write('<script src="js/jquery-3.2.1.min.js"><\/script>')</script>-->
        <script src="js/jquery-3.2.1.min.js"><\/script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>
        <script src="js/select2.min.js"></script>
        <script defer src="js/fontawesome-all.js"></script>
        <?php
    }
    public static function getLanguagesSelect()
    {
        $manager = new SqlManager();
        $manager->openConnection();
        $data = $manager->selectStarFrom('lingua');
        $manager->closeConnection();
        ?>
        <select class='form-control' id='languages' name='lingua'>
        <?php
            foreach ($data as $lingua)
            {
                ?>
                <option value='<?=$lingua['id']?>'><?=$lingua['country_name']?></option>
                <?php
            }
        ?>
        </select>
        <script>
            $(document).ready(function() {
                $('#languages').select2();
            });
        </script>
        <?php
        
    }
    public static function drwQuestionari()
    {
        $manager = new SqlManager();
        $manager->openConnection();
        $data = $manager->selectStarFrom('questionario','order by idQuestionario DESC');
        $manager->closeConnection();
        ?>
        <table  class='table table-hover table-striped'>
            <thead>
                <th>Questionario</th>
                <th>Modifica</th>
            </thead>
        <?php
        $first = true;
        foreach ($data as $questionario)
        {
            ?>
                <tr>
                    <td id="<?=$first ? "first" : "" ?>"><?=$questionario['descrizione']?></td>
                    <td><i class="far fa-edit"></i></td>
                </tr>
            <?php
            $first = false;
        }
        echo "</table>";
    }
    public static function drwaTableOf($table,$htmlId,$excluding)
    {
        $manager = new SqlManager();
        $manager->openConnection();
        $data = $manager->selectStarFrom($table);
        $manager->closeConnection();
        if (isset($data[0]))
        {
            $keys = array_keys($data[0]);
            echo "<table id='".$htmlId."' class='table diaplay'>";
            echo "<thead>";
            foreach ($keys as $key)
            {
                if (!in_array($key,$excluding))
                {
                    ?>
                    <th><?=$key?></th>
                    <?php
                }
            }
            echo "</thead>";
            $first = true;
            foreach ($data as $row)
            {
                ?>
                <tr id="<?=$first ? "first" : "" ?>">
                <?php
                foreach ($row as $key => $value)
                {
                    if (!in_array($key,$excluding))
                    {
                        ?>
                        <td ><?=$value?></td>
                        <?php
                    }
                }
                ?>
                </tr>
                <?php

            }
            echo "</table>";
        }
    }
}