<?php
class UserValidation
{
    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
	public static function generateToken($userid)
    {
        $token = bin2hex(openssl_random_pseudo_bytes(24));
        $sqlManager= new SqlManager;
        $sqlManager->openConnection();
        $sqlManager->saveAuthToken($userid,$token);
        $sqlManager->closeConnection();
        $arr=array('token'=>$token,'user_id'=>$userid);
        //$arrJson = json_encode($arr);
        $json = [
            "message" => "token generato",
            "priority" => "success",
            "result" => "ok",
            "data" => $arr
        ];
        return json_encode($json);
    }
    public static function extendToken($userId)
    {
        $sqlManager = new SqlManager;
        $sqlManager->openConnection();
        $update = $sqlManager->updateExpDate($userId);
        $sqlManager->closeConnection();
        return $update;
    }
    public function login($username, $password)
    {
        $sqlManager = new SqlManager;
        $sqlManager->openConnection();
        $auth=$sqlManager->checkUserPassword($username,$password);
        if ($auth)
           $token=self::generateToken($auth['userId']);
        else
            $token=json_encode(array('message'=>'Errore nelle credenziali', 'priority' => 'danger' , 'result' => 'ko' ));
        $sqlManager->closeConnection();
        return $token;
    }
}
    