<?php


class Helper
{
    public static function getLogger($logger = 'main')
    {

            Logger::configure(array(
                'appenders' => array(
                    'default' => array(
                        'class' => 'LoggerAppenderPDO',
                        'params' => array(
                            'dsn' => 'mysql:host=localhost;dbname=opera',
                            'user' => 'operaMySql',
                            'password' => 'TtOperaMySql',
                            'table' => 'log4php_log',
                        ),
                    ),
                ),
                'rootLogger' => array(
                    'appenders' => array('default'),
                ),
            ));
            $log = Logger::getLogger($logger);
            return $log;
    }
}