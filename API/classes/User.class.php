<?php
class User
{
	public $username;
	public $password;
	public $hash;
	public $admin;
	public $email;
	public $nome;
	public $cognome;

	public function __construct($data)
	{
		$this->username = $data['username'];
		$this->password = $data['password'];
		$this->nome = $data['nome'];
		$this->cognome = $data['cognome'];
		$this->email = $data['email'];
		$this->admin = $data['admin'];
		$this->hash = password_hash($data['password'],PASSWORD_BCRYPT);
	}

}
