<?php 
class DataList
{
    public function getScopes() //return JSON seller scopes defined in the db (goosell_sellerscope)
    {
        $sqlManager= new SqlManager();
        $sqlManager->openConnection();
        $scopes=$sqlManager->getScopes();
        $sqlManager->closeConnection();
        return json_encode($scopes);   
    }
    public function getCities()
    {
        $sqlManager= new SqlManager();
        $sqlManager->openConnection();
        $cities=$sqlManager->getCities();
        $sqlManager->closeConnection();
        return json_encode($cities); 
    }
}