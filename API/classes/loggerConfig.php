<?php
array(
    'appenders' => array(
        'default' => array(
            'class' => 'LoggerAppenderPDO',
            'params' => array(
                'dsn' => 'mysql:host=localhost;dbname=opera',
                'user' => 'operaMySql',
                'password' => 'TtOperaMySql',
                'table' => 'log',
            ),
        ),
    ),
    'rootLogger' => array(
        'appenders' => array('default'),
    ),
);