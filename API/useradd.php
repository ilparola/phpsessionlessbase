<?php
include ('classes\logger\Logger.php');
function my_autoloader($class) {
    include 'classes/' . $class . '.class.php';
}
spl_autoload_register('my_autoloader');
$log = Helper::getLogger();
$manager = new SqlManager();


if ($_POST)
{
    if (isset($_POST['username']))
    {

        $user = new User($_POST);
        $response = $manager->saveUser($user);
        if (json_decode($response)->result == "ko")
            Template::showAlertInDiv($response);
    }
}
else
{
    echo "{'message':'Nessun dato ricevuto!' , 'priority':'danger'}";
}