-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 19, 2018 alle 11:07
-- Versione del server: 5.7.14
-- Versione PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opera`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `authtoken`
--

CREATE TABLE `authtoken` (
  `user_id` int(11) NOT NULL,
  `token_key` text NOT NULL,
  `token_exp` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `authtoken`
--

INSERT INTO `authtoken` (`user_id`, `token_key`, `token_exp`) VALUES
(1, '8f697e10fbe0c91da05ddb25e0bb6b60d3ec047c4d9e8ac4', '10:25:14');

-- --------------------------------------------------------

--
-- Struttura della tabella `domanda`
--

CREATE TABLE `domanda` (
  `id` int(11) NOT NULL,
  `id_questionario` int(11) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `id_tipologia` int(11) DEFAULT NULL,
  `testo` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lingua`
--

CREATE TABLE `lingua` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `log4php_log`
--

CREATE TABLE `log4php_log` (
  `timestamp` datetime DEFAULT NULL,
  `logger` varchar(256) DEFAULT NULL,
  `level` varchar(32) DEFAULT NULL,
  `message` varchar(4000) DEFAULT NULL,
  `thread` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `line` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `log4php_log`
--

INSERT INTO `log4php_log` (`timestamp`, `logger`, `level`, `message`, `thread`, `file`, `line`) VALUES
('2018-01-17 11:07:29', 'main', 'INFO', 'hello!', 1888, 'C:\\wamp64\\www\\operaApi\\index.php', '22'),
('2018-01-17 11:15:22', 'main', 'INFO', 'hello213!', 1888, 'C:\\wamp64\\www\\operaApi\\index.php', '10'),
('2018-01-17 11:17:10', 'main', 'INFO', 'hello213!', 1888, 'C:\\wamp64\\www\\operaApi\\index.php', '10');

-- --------------------------------------------------------

--
-- Struttura della tabella `questionario`
--

CREATE TABLE `questionario` (
  `idQuestionario` int(11) NOT NULL,
  `descrizione` text,
  `id_utente` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `risposta_domanda`
--

CREATE TABLE `risposta_domanda` (
  `id` int(11) NOT NULL,
  `id_domanda` int(11) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `testo` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `scelta`
--

CREATE TABLE `scelta` (
  `id` int(11) NOT NULL,
  `id_risposta` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `tipologia_domanda`
--

CREATE TABLE `tipologia_domanda` (
  `id` int(11) NOT NULL,
  `descrizione` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `userId` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `UUID` text,
  `admin` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `cognome` varchar(50) DEFAULT NULL,
  `lastRequest` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `user`
--

INSERT INTO `user` (`userId`, `username`, `password`, `UUID`, `admin`, `email`, `nome`, `cognome`, `lastRequest`) VALUES
(1, 'Admin', '$2y$10$/hitoaNqijW2kTleSZxT5ugK4ytXPTh3quJgFA2jtSVB0j5Ps1I3S', NULL, 1, 'andrea.landi@tecnosistemi.com', 'Andrea', 'Landi', NULL),
(3, 'test', '$2y$10$YWkK4xRTq2Bad5c8AaXc1Oh5q3bGOYZjUFb.J9vH8EsrkiiH.lRkO', NULL, 0, 'mariorossi@gmail.com', 'Mario', 'Rossi', NULL);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `domanda`
--
ALTER TABLE `domanda`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lingua`
--
ALTER TABLE `lingua`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `questionario`
--
ALTER TABLE `questionario`
  ADD PRIMARY KEY (`idQuestionario`);

--
-- Indici per le tabelle `risposta_domanda`
--
ALTER TABLE `risposta_domanda`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `scelta`
--
ALTER TABLE `scelta`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `tipologia_domanda`
--
ALTER TABLE `tipologia_domanda`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `domanda`
--
ALTER TABLE `domanda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lingua`
--
ALTER TABLE `lingua`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `questionario`
--
ALTER TABLE `questionario`
  MODIFY `idQuestionario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `risposta_domanda`
--
ALTER TABLE `risposta_domanda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `scelta`
--
ALTER TABLE `scelta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `tipologia_domanda`
--
ALTER TABLE `tipologia_domanda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
