<?php
session_start();
include ('API\classes\logger\logger.php');
function my_autoloader($class) {
    include 'API/classes/' . $class . '.class.php';
}
spl_autoload_register('my_autoloader');
$log = Helper::getLogger();
$manager = new SqlManager();
if (isset($_SESSION['auth_token']) && $manager->isTokenValid($_SESSION['user_id'],$_SESSION['auth_token']))
{
    UserValidation::extendToken($_SESSION['user_id']);
//$log->info("hello213!");
?>
    <!doctype html>
    <html lang="en">
    <head>
        <?php 
            Template::getHeader();
        ?>
    </head>
    <body>
        <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Home</a>
        </header>
        <div class="container-fluid">
            <div class="row">
                <?php 
                Template::getMenu();
                ?>
                <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                    <h1>Gestione Utenti</h1>
                    <h3><a  class="btn btn-primary" data-toggle="collapse" href="#formcollapse" role="button" aria-expanded="false" aria-controls="formcollapse">+ Inserisci nuovo utente</a></h3>
                    <div class="collapse" id="formcollapse">
                        <form id="addUserForm">
                            <input type="text" name="username" class="form-control" required="required" placeholder="username">
                            <input type="password" name="password" class="form-control" required="required" placeholder="password">
                            <input type="text" name="nome" class="form-control" required="required" placeholder="Nome">
                            <input type="text" name="cognome" class="form-control" required="required" placeholder="cognome">
                            <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                            <select class="form-control" name="admin">
                                <option value="0">User</option>
                                <option value="1">Admin</option>
                            </select>
                            <br>
                            
                        </form>
                        <div class="card" id="response"></div>
                        <button class="btn btn-primary" id="inserisciUtente">Salva</button>
                    </div>
                    <hr>
                    <h3>Elenco Utenti</h3>
                    <div id="listautenti">
                    <?php
                    Template::drwaTableOf('user','utenti',['password','lastRequest']);
                    ?>
                    </div>
                </main>
            </div>
        </div>
        <?php
            Template::getFooter();
        ?>
    </body>
    </html>
    <script>
    $( "#inserisciUtente" ).on( "click", function() {
            res = $.post('API/useradd.php',$('#addUserForm').serialize(),function(data) {
            $("#response").html( data );
            $("#utenti").load( "API/livepages/listuser.php");
        });
    });
    $(document).ready(function() {
        $('#utenti').DataTable();
    } );
    </script>
<?php
}
else
{
    header("location: index.php");
}
 